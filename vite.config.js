import { defineConfig } from 'vite'

const { resolve } = require('path')
// https://vitejs.dev/config/
export default defineConfig({
  build: {
    target:['es2015', 'chrome61'],
    lib: {
      entry: 'src/my-element.js',
      formats: ['es']
    },
    rollupOptions: {
      external: /^lit-element/,
      input: {
        main: resolve(__dirname, 'index.html'),
        screen: resolve(__dirname, 'screen/index.html')
      }
    }
  },
  css: {
    preprocessorOptions: {
      less: {
        javascriptEnabled: true,
      }
    },
  },
})
