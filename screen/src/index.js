import './tab-menu';
import '../styles/index.less'

function Screen() {
  this.initView = () => {
    this.$screenHeader = document.querySelector('.screen-container-header');
    this.$screenBody = document.querySelector('.screen-container-body');
    this.$toggleBar = document.querySelector('.toggle-bar');
    this.$iframe = document.querySelector('iframe');
  }
  this.init = () => {
    this.initView();
    this.initEvent();
  }
  this.initEvent = () => {
    this.$toggleBar.addEventListener('click', () => {
      this.toggleView(true);
    });
    this.$screenBody.addEventListener('click', () => {
      this.toggleView(false);
    })
  }
  this.toggleView = (flag) => {
    if (this.$screenHeader.style.height === '0px' && flag) {
      this.$screenHeader.style.height = '200px';
      this.$toggleBar.classList.value = 'toggle-bar';
      this.$screenBody.classList.value = 'screen-container-body';
    } else {
      this.$screenHeader.style.height = 0;
      this.$screenBody.classList.value = 'screen-container-body active';
      this.$toggleBar.classList.value = 'toggle-bar active';
    }
  }
}