import {
  LitElement,
  html,
  css
} from 'lit-element'
/**
 * 轮播模式组件
 *
 * @slot - This element has a slot
 * @csspart button - The button
 */
export class turnMode extends LitElement {
  static get properties() {
    return {
      localMenu: {
        type: Object,
        reflect: true
      },
      currentType: {
        type: String
      },
      myEvent:{
        attribute: false
      }
    }
  }
  static get styles() {
    return css `.turn-box{
      display: flex;
      flex-wrap: wrap;
      width: 400px
    }
    .turn-box .box-item{
      position: relative;
      width: 90px;
      border: 1px solid;
      margin: 5px 5px 0 0;
    }
    .box-item .delete{
      position: absolute;
      top: 0;
      right: 5px;
      cursor: pointer;
    }`
  }
  constructor(localMenu, currentType, myEvent) {
    super();
    this.localMenu = localMenu;
    this.currentType = currentType;
    this.myEvent = myEvent;
  };

  get $confirmButton() {
    return this.renderRoot.querySelector('.confirm-button');
  }
  get $turnInput(){
    return this.renderRoot.querySelector('.turn input');
  }
  get $parent(){
    return this.renderRoot.ownerDocument.querySelector('tab-menu');
  }

  _deleteTurn(e) {
    const {
      index
    } = e.target.dataset;
    this.localMenu.turnList.splice(index, 1);
    this.requestUpdate();
    this.$parent.requestUpdate();
  }
  _initEvent(){
    // 第三个视图确认按钮
    this.$confirmButton.addEventListener('click', (e) => {
      if(this.$turnInput.value === ''){
        return alert('切换时间不能为空');
      }
      if(this.$turnInput.value < 5){
        return alert('切换时间不能小于5秒');
      }
      if(this.localMenu.turnList.length < 2){
        return alert('轮播的视图不能低于2个');
      }
      this.$parent._saveMenu();
      console.log(this.renderRoot.ownerDocument.screen)
    });
  }
  attributeChangedCallback(value){
    super.attributeChangedCallback();
  }
  update(){
    super.update();
  }
  firstUpdated(){
    super.firstUpdated();
    this._initEvent();
  }
  connectedCallback(){
    super.connectedCallback();
  }
  render() {
    return html `
      <div class="mode-type turn ${this.currentType=='turn'?'active':''}">
      <header>
        <span>请输入轮播切换的时间</span>
        <input type="number" placeholder="轮播切换的时间" maxlength="2" value="10">
        <button class="confirm-button">确认</button>
      </header>
      <section class="turn-box">
      <div class="tips" style="display:${this.localMenu.turnList?.length?'none':'block'}">请点击右侧视图，选择轮播的视图</div>
      ${this.localMenu.turnList?.map((item,i)=>{
        return html`
        <div class="box-item">
        ${item.name}
        <span class="delete" data-index="${i}" @click="${this._deleteTurn}">x</span>
        </div>`
      })}
      </section>
      </div>`
  }
}

window.customElements.define('turn-mode', turnMode)