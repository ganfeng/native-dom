import { LitElement, html, css } from 'lit-element'

/**
 * An example element.
 *
 * @slot - This element has a slot
 * @csspart button - The button
 */
export class radarElement extends LitElement {
  static get styles() {
    return css`
      :host {
        
      }
      .radar{
        width:100px;
        height:100px;
        border:1px solid #9bfdfd;
        border-radius: 50%;
      }
      .radar:after{
        position: relative;
        top: -150px;
        display: block;
        content:'';
        height: 50px;
        width: 100px;
        border-bottom: 1px solid #9bfdfd;
      }

      .radar:before{
        display: block;
        content:'';
        height: 100px;
        width: 50px;
        border-right: 1px solid #9bfdfd;
      }
      .fan{
        width: 50px;
        height: 50px;
        border: 1px solid #9bfdfd;
        position: relative;
        top: -75px;
        left: 25px;
        border-radius: 50%;
      }
      .fan::before{
        display: block;
        content: "";
        height: 50px;
        /* border-left: 1px solid red; */
        position: relative;
        top: -25px;
        left: 25px;
        /* box-shadow: red 0px 0px 5px; */
        border-radius: 0px 100% 0px 0px;
        background-image: linear-gradient(to right, transparent, #9bfdfd);
        transform-origin: 0 100%;
        animation: turn 2s linear infinite;
      }
      @keyframes turn{
        from{
          transform: rotate(0deg)
        }
        to{
          transform: rotate(360deg)
        }
      }
    `
  }

  constructor() {
    super()
  }

  render() {
    return html`
    <div class="radar">
      <div class="fan"></div>
    </div>
    `
  }
}

window.customElements.define('radar-element', radarElement)
