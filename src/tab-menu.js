import { LitElement, html, css } from 'lit-element'

/**
 * An example element.
 *
 * @slot - This element has a slot
 * @csspart button - The button
 */
export class tabMenu extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        border: solid 1px gray;
        padding: 16px;
        max-width: 800px;
      }
      .tab-menu-container{
        display: flex;
        list-style: none;
        margin: 0;
        padding: 0;
      }
      li{
        padding: 0 1em;
        cursor: pointer;
      }
      li:hover,
      li.active{
        border-bottom: 1px solid;
      }
    `
  }
  static get properties() {
    return {
      menus: { type: Array },
      currentItem: { type: String },

    }
  }

  constructor() {
    super()
    this.menus = ['test1', 'test2'];
    this.currentItem = 'test1';
  }

  render() {
    return html`
      <ul class="tab-menu-container">
        ${this.menus.map(i => 
          html`<li 
          class="item ${this.currentItem==i?'active':''}" 
          @click=${()=>{this._changeItem(i)}}>
          ${i}
          </li>`)} 
      </ul>
      <div>
        ${this.currentItem}
      </div> 
    `
  }

  _changeItem(i) {
    console.log(i)
    this.currentItem = i;
  }
}

window.customElements.define('tab-menu', tabMenu)
