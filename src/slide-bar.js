import { LitElement, html, css } from 'lit-element'
/**
 * 一个滑动的数值组件.
 *
 * @slot - This element has a slot
 * @csspart button - The button
 */
export class slideBar extends LitElement {
  static get properties() {
    return {
      value: { type: Number },
      text: { type: String },

    }
  }

  constructor(value, text) {
    super()
    this.value = value;
    this.text = text;
  };

  _initEvent() {
    let slideAreaWidth = this._bg.offsetWidth;
    this._point.onmousedown = e => {
      let distanceX = e.clientX - this._point.offsetLeft;
      this._bg.onmousemove = e => {
        let left = e.clientX - distanceX;
        if (left <= 0) {
          left = 0;
        }
        if (left >= slideAreaWidth) {
          left = slideAreaWidth;
        }
        let currentValue = parseInt(left / slideAreaWidth * 100);
        this._point.style.left = currentValue + '%';
        this._acitve.style.width = currentValue + '%';
        // lastValue = lastValue + moveX / w;
        function handleBoundValue(value) {k
          value = parseFloat(value, 2)
          if (value < 0) {
            return '0%'
          }
          if (value > 100) {
            return '100%';
          }
          return value + '%';
        }
      };
    };
    this._bg.onmouseleave = e => {
      this._bg.onmousemove = null;
    }
    this._point.onmouseup = e => {
      this._bg.onmousemove = null;
    };

  }
  update() {
    super.update();
    this._initEvent();
  }
  // connectedCallback() {
  //   super.connectedCallback();
  //   console.log('connectedCallback')
  //   console.log(this._point.addEventListener)
  // }
  // disconnectedCallback() {
  //   console.log('disconnectedCallback')
  //   console.log(this._point)
  // }
  // adoptedCallback() {
  //   console.log('adoptedCallback')
  //   console.log(this._point.addEventListener)
  // }
  // attributeChangedCallback() {
  //   console.log('attributeChangedCallback')
  //   console.log(this._point)
  // }
  render() {
    console.log('render')
    return html`
      <div id="slideBar" class="slide-bar-bg">
          <div class="slide-bar-active"></div> 
         <input type="hidden" value="${this.value}">
         <a class="slide-bar-point"></a>
         <span class="text">${this.value}</span>
      </div> 
    `
  }
  get _point() {
    return this.renderRoot.querySelector('a');
  }
  get _acitve() {
    return this.renderRoot.querySelector('.slide-bar-active');
  }
  get _bg() {
    return this.renderRoot.querySelector('.slide-bar-bg');
  }
  static get styles() {
    return css`
      :host {
        display: block;
        border: solid 1px gray;
        padding: 16px;
        max-width: 800px;
      }
      .slide-bar-bg{
        position: relative;
        height: 20px;
        background-color: rgba(0,0,0,.1);
      }
      .slide-bar-bg:after{
        position: absolute;
        top: 50%;
        display:block;
        width: 100%;
        height: 2px;
        background-color: #000;
        content:'';
      }
      .slide-bar-active{
        position: absolute;
        top: 50%;
        width: 0;
        height: 2px;
        background-color: #fff;
        z-index: 1;
      }
      .slide-bar-point{
        position: absolute;
        top: calc(50% - 5px);
        left: 0;
        width: 10px;
        height: 10px;
        background: #000;
        border-radius: 50%;
        z-index: 2;
        cursor: pointer;
      }
      .text{
        position: absolute;
        top: 100%;

      }
    `
  }
}

window.customElements.define('slide-bar', slideBar);
