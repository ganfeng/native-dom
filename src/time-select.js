import { LitElement, html, css } from 'lit-element'

export class TimeSelect extends LitElement{
    constructor(){
        super();
        this.type = '0';
        this.timeText = '-';
    }
    update(){
        super.update();

    }
    render(){
        return html`
        <div class="time-select">
            <div id="timeText></div>
            <div>
                <select>
                    
                </select>
                <div class="time-selector"></div>
            </div>
        </div>
        `
    }
}